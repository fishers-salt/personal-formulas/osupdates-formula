# frozen_string_literal: true

control 'osupdates-package-update-pkgs-uptodate' do
  impact 'high'
  title 'Installed packages are up to date'

  platform = system.platform[:family]

  case platform
  when 'arch'
    describe command("sudo pacman --sync --refresh --refresh && echo #{platform}") do
      its('stdout') { should match '.*Synchronizing package databases.*' }
      its('stderr') { should eq '' }
      its('exit_status') { should eq 0 }
    end

    describe command('sudo pacman --query --upgrades') do
      its('stdout') { should be_empty }
      its('stderr') { should be_empty }
      its('exit_status') { should eq 1 }
    end
  when 'fedora'
    describe command('sudo dnf check-update') do
      its('exit_status') { should eq 0 }
    end
  else
    describe command("sudo apt-get update && echo #{platform}") do
      its('stdout') { should match '.*\nReading package lists.*' }
      its('stderr') { should eq '' }
      its('exit_status') { should eq 0 }
    end

    describe command('sudo apt-get --show-upgraded upgrade --assume-no') do
      its('stdout') { should match '0 upgraded' }
      its('stderr') { should eq '' }
      its('exit_status') { should eq 0 }
    end
  end
end
